# LTT Tabemono Booking

Công ty thường có đặt đồ ăn theo nhóm (thường là đặt chè) và đặt cơm vào buổi trưa hằng ngày (từ thứ 2 đến thứ 6).

Hiện tại thì mọi người đang đặt bằng cách khá là thủ công là đặt bằng google sheet. Đặt như thế này thì khá là thủ công và không dễ dàng trong việc tra cứu lại lịch sử.

Ngoài ra người khác muốn đặt một một đơn hàng tương tự như vậy thì lại phải lục lại sheet đã tạo.

Nên anh muốn viết một cái web để đặt đồ ăn có một số tính năng nổi bật như:

- Tạo menu vào chia sẻ menu.
- Thực hiện đặt đồ ăn theo menu, tổng kết số tiền, người đặt, số lượng v.v...
- Thông báo cho mọi người, có thể là toàn bộ, hoặc theo team.
- Xuất excel

## Công nghệ sử dụng

- Laravel 7.2.5
- VueJS
- Mysql

## Hướng dẫn

Codebase anh đã dựng sẵn ở Repository này rồi, các em clone về và rồi làm theo các bước bên dưới để chạy nhé. Phần Codebase gần như là giống y hệt với laravel gốc.

Anh chỉ bổ sung thêm docker và chỉnh sửa một chút thông số.

1. Cài đặt docker & docker-compose (không rõ thì qua hỏi anh)
2. Chạy câu lệnh
```sh
docker-compose run --rm composer install
```
Để sử dụng **composer** cài các thư viện cho laravel

3. Chạy câu lệnh
```sh
cp .env.example .env
```
Để tạo file **.env** từ file **.env.example**

4. Chạy câu lệnh
```sh
docker-compose run --rm artisan key:generate
```
Để tạo **APP_KEY** vào file **.env**

5. Chạy câu lệnh
```sh
sudo chmod 777 -R storage/
```
Để set lại quyền cho folder **storage**, mục đính để cho docker có thể lưu được **cache** và **logs** mà không bị lỗi *permission denied*

6. Chạy câu lệnh
```sh
docker-compose up
```
Sau đó truy cập vào http://localhost:800/ đối với web

Còn database thì:
```
Username: root
Password: example
Ports: 33060
```

_Nếu trong quá trình cài đặt có vấn đề gì thì báo anh nhé_

Vì chạy bằng docker nên khi cần sự dụng **artisan** hay **composer** sẽ hơn khác với bình thường các em hay chạy.

Đơn giản là chỉ cần thêm đoạn: **docker-compose run --rm** ở trước mỗi câu lệnh là được

VD:
```
php artisan make:migration create_users_table
```
sẽ được thay thế bằng:
```
docker-compose run --rm artisan make:migration create_users_table
```

## Gitflow

tất cả branch mới được phải được tạo ra từ branch **develop**

Branch mới được tạo để phát triển một tính năng thì tên phải bắt đầu bằng **feature/**. VD như: feature/login

Branch mới được tạo để fix bug thì tên phải bắt đầu bằng **fixbug/**.

Sau khi code xong thì phải tạo Pull request vào branch **develop**.